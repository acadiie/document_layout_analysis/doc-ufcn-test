# Experiments on the INED Sample dataset with Teklia’s `doc-ufcn`

![Python 3.8](https://img.shields.io/badge/Python_Version-3.8-success)

## Installation

The scripts should be installed in your environment using pip by including the following line in your `requirements.txt` file.

```text
git+ssh://git@gitlab.univ-lr.fr/acadiie/document_layout_analysis/doc-ufcn-test.git#egg=doc-ufcn-test
```

## Run `doc-ufcn` on a folder of images

```bash
Run the doc-ufcn on a directory of images.

optional arguments:
  -h, --help            show this help message and exit
  --input-directory INPUT_DIRECTORY, -i INPUT_DIRECTORY
                        Directory of JPEG images to process.
  --output-directory OUTPUT_DIRECTORY, -o OUTPUT_DIRECTORY
                        Directory where to store the model outputs.
  --model-name MODEL_NAME, -m MODEL_NAME
                        Path to an existing model (.pth) or name of a generic model that will be downloaded from HuggingFace.
  --model-config MODEL_CONFIG
                        Path to the JSON file with configuration data for the model. The document has four keys: ’nb_of_classes’, ‘input_size’, ‘mean’ and ‘std’.
  --mask MASK           Whether to export the label mask image
  --overlap OVERLAP     Whether to export the overlap image
  --polygons POLYGONS   Whether to export the polygons found in the image in .json files
```

### Example

The example below will run the `doc-ufcn` model on the test images with the [`generic-page`](https://huggingface.co/Teklia/doc-ufcn-generic-page) model.

```bash
doc-ufcn-test -i library_tests/test_images -o output_test_images -m generic-page
```