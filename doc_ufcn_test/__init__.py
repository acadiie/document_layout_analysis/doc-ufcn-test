import json
import logging
import sys
from json import JSONEncoder
from pathlib import Path
from typing import Any, Dict, List, Tuple

import cv2
import numpy
from doc_ufcn import models
from doc_ufcn.main import DocUFCN
from matplotlib import pyplot

logging.basicConfig(
    format="[%(levelname)s] %(message)s",
    stream=sys.stdout,
)


def load_model_from_file(
    file: Path,
    nb_of_classes: int,
    mean: List[int],
    std: List[int],
    input_size: int,
    device: str = "cpu",
) -> "DocUFCN":
    """
    If the model file has been downloaded manually, you can load it here.
    Model parameters should be filled manually.
    :param file: path to the file to load
    :param nb_of_classes: number of classes the model has been trained on
    :param mean: of input data
    :param std: of input data
    :param input_size: input features size
    :param device: which device to run on using pytorch: cpu or cuda:0 for instance.
    :return: the loaded model
    """
    loaded_model = DocUFCN(nb_of_classes, input_size, device)
    loaded_model.load(file, mean, std, mode="eval")
    return loaded_model


def load_model_from_name(hugging_face_name: str, device: str = "cpu") -> "DocUFCN":
    """
    Download a model from HuggingFace based on its name.
    :param hugging_face_name: name of the model to download
    :param device: which device to run on using pytorch: cpu or cuda:0 for instance.
    :return: the loaded model
    """
    loaded_model_path, loaded_model_parameters = models.download_model(hugging_face_name)
    loaded_model = DocUFCN(
        len(loaded_model_parameters["classes"]),
        loaded_model_parameters["input_size"],
        device,
    )
    loaded_model.load(
        loaded_model_path,
        loaded_model_parameters["mean"],
        loaded_model_parameters["std"],
    )
    return loaded_model


def load_model_from_filename(model_path: Path, model_config: Dict[str, Any], device: str = "cpu"):
    print(f"Using configuration: {model_config}")
    loaded_model = DocUFCN(model_config.get("nb_of_classes"), model_config.get("input_size"), device)
    loaded_model.load(model_path, model_config.get("mean"), model_config.get("std"))
    return loaded_model


def load_image_from_filename(file: Path) -> "cv2.cvtColor":
    """
    Load an image for the model from a filename
    :param file: path to the file to read
    :return: the image in OpenCV cvtColor file.
    """
    return cv2.cvtColor(cv2.imread(file), cv2.COLOR_BGR2RGB)


def compute_with_model_for_image(
    image: "cv2.cvtColor", model: "DocUFCN"
) -> Tuple[Dict, Dict, "cv2.cvtColor", "cv2.cvtColor"]:
    """
    Compute model output for a given image.
    :return:
    """
    detected_polygons, probabilities, mask, overlap = model.predict(
        image, raw_output=True, mask_output=True, overlap_output=True
    )
    return detected_polygons, probabilities, mask, overlap


class NumpyArrayEncoder(JSONEncoder):
    """Encoder used to dump dict of numpy array into JSON"""

    def default(self, obj):
        if isinstance(obj, numpy.int64):
            return int(obj)
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


def export_model_output(image, model, filename_base: str, dirname: Path):
    detected_polygons, probabilities, mask, overlap = compute_with_model_for_image(image.copy(), model)
    detected_polygons["img_size"] = [image.shape[0], image.shape[1]]

    dirname.mkdir(parents=True, exist_ok=True)

    images_dir = Path(dirname) / "images"
    images_dir.mkdir(parents=True, exist_ok=True)
    pyplot.clf()
    pyplot.imsave(images_dir / f"{filename_base}.png", image)
    pyplot.close()

    labels_json_dir = Path(dirname) / "labels_json"
    labels_json_dir.mkdir(parents=True, exist_ok=True)
    with open(labels_json_dir / f"{filename_base}.json", mode="w") as polygons_fd:
        json.dump(detected_polygons, polygons_fd, indent=2, cls=NumpyArrayEncoder)

    labels_dir = Path(dirname) / "labels"
    labels_dir.mkdir(parents=True, exist_ok=True)
    pyplot.clf()
    pyplot.imsave(labels_dir / f"{filename_base}.png", mask / 255, cmap="gray")
    pyplot.close()
