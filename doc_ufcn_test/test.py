#!/usr/bin/env python3
import argparse
import glob
import json
import time
from pathlib import Path

from tqdm import tqdm

from doc_ufcn_test import (
    export_model_output,
    load_image_from_filename,
    load_model_from_filename,
    load_model_from_name,
)


def main():
    parser = argparse.ArgumentParser(description="Run the doc-ufcn on a directory of images.")
    parser.add_argument(
        "--input-directory", "-i", help="Directory of JPEG images to process.", type=str, required=True
    )
    parser.add_argument(
        "--output-directory", "-o", help="Directory where to store the model outputs.", type=str, required=True
    )
    parser.add_argument(
        "--model-name",
        "-m",
        help="Path to an existing model (.pth) or name of a generic model that will "
        "be downloaded from HuggingFace.",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--model-config",
        help="Path to the JSON file with configuration data for the model. "
        "The document has four keys: ’nb_of_classes’, ‘input_size’, ‘mean’ and ‘std’.",
        type=str,
        required=False,
    )
    parser.add_argument("--mask", help="Whether to export the label mask image", type=bool, default=True)
    parser.add_argument("--overlap", help="Whether to export the overlap image", type=bool, default=False)
    parser.add_argument(
        "--polygons", help="Whether to export the polygons found in the image in .json files", type=bool, default=False
    )
    args = parser.parse_args()

    # Check whether it’s possible to load the model.
    model_name = Path(args.model_name)
    if model_name.exists():
        if Path(args.model_name).suffix == ".pth" and args.model_config is not None:
            with open(args.model_config, mode="r") as model_json_fd:
                model = load_model_from_filename(model_name, json.load(model_json_fd), device="cuda")
        else:
            ValueError("Using a local file without any configuration is forbidden. Use configuration JSON file.")
            exit(-1)
    else:
        model = load_model_from_name(str(model_name), device="cuda")

    # Load list of files to process
    image_files = set(glob.glob(f"{args.input_directory}/*.png"))

    start_time = time.time()
    output_directory = Path(args.output_directory)

    for current_image_path in tqdm(image_files, desc=f"Using {model_name.name}: "):
        file = Path(current_image_path)
        image = load_image_from_filename(current_image_path)
        export_model_output(
            image,
            model,
            file.stem,  # remove suffix .jpg
            output_directory,
        )

    end_time = time.time()

    fps = len(image_files) / (end_time - start_time)
    with open(output_directory / "fps.txt", mode="w") as of:
        of.write(f"{fps:.2f}\n")


if __name__ == "__main__":
    main()
